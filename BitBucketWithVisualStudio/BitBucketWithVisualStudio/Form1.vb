﻿Public Class frmBitBucket
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'declare variables
        Dim dblInput As Double
        'take input
        dblInput = Convert.ToDouble(txtInput.Text)
        'test if odd/even, print result 
        If dblInput Mod 2 = 0 Then
            lblOutput.Text = "EVEN"
        Else
            lblOutput.Text = "ODD"
        End If
    End Sub
End Class
